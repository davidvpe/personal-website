import AboutMe from '../components/aboutme'
import Home from '../components/home'
import PropTypes from 'prop-types'
import Head from 'next/head'
import Services from '../components/services'
import Resume from '../components/resume'

const Link = ({ children, link }) => {
    return (
        <a
            target='_blank'
            rel='noreferrer'
            className='underline hover:text-gray transition-all duration-100'
            href={link}
        >
            {children}
        </a>
    )
}

Link.propTypes = {
    children: PropTypes.any.isRequired,
    link: PropTypes.string.isRequired,
}

export default function Portfolio() {
    return (
        <>
            <Head>
                <title>David Velarde Portfolio</title>
            </Head>
            <Home></Home>
            <AboutMe></AboutMe>
            <Services></Services>
            <Resume></Resume>
            <div className='fixed top-0 w-full text-center bg-green font-sans p-5'>
                This is currently under development. To check the progress
                please visit the{' '}
                <Link link='https://gitlab.com/davidvpe/personal-website'>
                    Gitlab Repository
                </Link>{' '}
                or the{' '}
                <Link link='https://gitlab.com/davidvpe/personal-website/-/boards'>
                    Development Board
                </Link>
            </div>
        </>
    )
}
