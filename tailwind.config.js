const colors = require('tailwindcss/colors')
module.exports = {
    purge: [
        './pages/**/*.{js,ts,jsx,tsx}',
        './components/**/*.{js,ts,jsx,tsx}',
    ],
    darkMode: false, // or 'media' or 'class'
    theme: {
        colors: {
            green: {
                DEFAULT: '#20c997',
                dark: '#1baa80',
            },
            red: colors.red[500],
            black: { DEFAULT: '#212529', light: '#343a40' },
            gray: colors.gray[500],
            white: colors.white,
        },
        extend: {
            backgroundImage: (theme) => ({
                'home-background':
                    "url('https://static-davidvpe.s3.us-west-2.amazonaws.com/portfolio-home-background.jpg')",
            }),
        },
    },
    variants: {
        extend: {},
    },
    plugins: [],
}
