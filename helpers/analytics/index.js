import * as ga from '../../lib/ga'

const sendEvent = (action, section) => {
    if (process.env.NODE_ENV != 'production') return
    ga.event({
        action: 'click',
        params: { event_category: section, event_label: action },
    })
}

export const HomeEvents = {
    hireMe: () => {
        sendEvent('sendMail', 'home')
    },
}

export const AboutMeEvents = {
    downloadCV: () => {
        sendEvent('downloadCV', 'aboutme')
    },
}
