import { HomeEvents } from '../analytics'

export const openEmail = () => {
    HomeEvents.hireMe()

    const mail = Buffer.from(
        'Y29udGFjdG9AY29wc3RvbmUubmV0',
        'base64'
    ).toString()

    location.href = `mailto:${mail}`
}
