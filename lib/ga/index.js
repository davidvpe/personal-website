// log the pageview with their URL

export const gaID = 'UA-40949276-12'

export const pageview = (url) => {
    window.gtag('config', gaID, {
        page_path: url,
    })
}

// log specific events happening.
export const event = ({ action, params }) => {
    window.gtag('event', action, params)
}
