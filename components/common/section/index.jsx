import React from 'react'
import PropTypes from 'prop-types'
import { SectionTitle } from '../texts/index'
import styles from './section.module.css'

const Section = (props) => {
    return (
        <section
            className={
                props.type === 'dark'
                    ? styles.section_dark
                    : styles.section_light
            }
        >
            <div className={styles.section_container}>
                <SectionTitle
                    background={props.backTitle}
                    title={props.title}
                />
                {props.children}
            </div>
        </section>
    )
}

Section.propTypes = {
    type: PropTypes.oneOf(['dark', 'light']).isRequired,
    backTitle: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node,
    ]),
}

export default Section
