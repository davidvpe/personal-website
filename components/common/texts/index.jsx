import React from 'react'
import PropTypes from 'prop-types'
import sectionTitleStyles from './section_title.module.css'

const SectionTitle = ({ background, title }) => {
    return (
        <div
            background={background}
            className={sectionTitleStyles.title_container}
        >
            <h1 className={sectionTitleStyles.title}>
                {title}
                <span className={sectionTitleStyles.bottom_stroke}></span>
            </h1>
        </div>
    )
}

SectionTitle.propTypes = {
    background: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
}

export { SectionTitle }
