import React from 'react'
import styles from './buttons.module.css'
import PropTypes from 'prop-types'

export const cvHref =
    'https://static-davidvpe.s3.us-west-2.amazonaws.com/david_velarde_resume.pdf'

const RoundedButton = ({ href, onClick, title, type }) => {
    let buttonStyle = ''
    switch (type) {
        case 'active':
            buttonStyle = styles.active_button
            break
        case 'alt':
            buttonStyle = styles.alt_button
            break
        case 'greyActive':
            buttonStyle = styles.greyActive_button
            break
    }

    return (
        <a
            target='_blank'
            rel='noreferrer'
            href={href ? href : '#'}
            onClick={onClick}
            className={buttonStyle}
        >
            {title}
        </a>
    )
}

RoundedButton.propTypes = {
    href: PropTypes.string,
    onClick: PropTypes.func.isRequired,
    title: PropTypes.string.isRequired,
    type: PropTypes.oneOf(['active', 'alt', 'greyActive']),
}

export default RoundedButton
