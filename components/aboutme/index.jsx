import React from 'react'
import { AboutMeEvents } from '../../helpers/analytics'
import RoundedButton, { cvHref } from '../common/buttons'
import Section from '../common/section'
import styles from './aboutme.module.css'
import InfoRow from './info'

const AboutMe = () => {
    const currentAge = new Date().getFullYear() - 1993

    return (
        <Section backTitle='about me' title='Know Me More' type='dark'>
            <div className={styles.content_container}>
                <h3 className={styles.title_name}>
                    {"I'm"} <span className='text-green'>David Velarde</span>, a
                    Full Stack Developer
                </h3>
                <p>
                    I can help you build the website or web app your company
                    needs, from simple landing pages, all the way to complex
                    administration systems taking advantage of the cloud.
                </p>
                <p>
                    I have 3 years of experience in Frontend and Backend Web
                    Development, going from Python to Javascript. I’ve worked
                    mainly with Django, Flask, ReactJS, ExpressJS, KoaJS,
                    TailwindCSS, SASS. I also have experience with CI/CD tech
                    stack, such as Jenkins, GitlabCI. And I’ve worked doing
                    deployments in Cloud Environments such as AWS and
                    DigitalOcean.
                </p>
                <p>
                    {"I'm"} also an iOS Developer with over 10 years of
                    experience working with several companies and project types
                    around the world, from supermarkets to delivery apps.
                </p>
            </div>
            <div className={styles.personalInfo}>
                <ul>
                    <InfoRow infoKey='Name' infoValue='David Velarde'></InfoRow>
                    <InfoRow infoKey='Email' infoValue='Hidden'></InfoRow>
                    <InfoRow infoKey='Age' infoValue={currentAge}></InfoRow>
                    <InfoRow infoKey='From' infoValue='Peru'></InfoRow>
                </ul>
                <RoundedButton
                    href={cvHref}
                    onClick={AboutMeEvents.downloadCV}
                    type='active'
                    title='Download CV'
                />
            </div>
        </Section>
    )
}

export default AboutMe
