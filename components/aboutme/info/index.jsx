import React from 'react'
import styles from './info.module.css'
import PropTypes from 'prop-types'
import { openEmail } from '../../../helpers/email'
const InfoRow = (props) => {
    return (
        <li className={styles.info}>
            {props.infoKey}:
            {props.infoKey === 'Email' ? (
                <span
                    onClick={openEmail}
                    className={`${styles.mail} ${styles.normal}`}
                >
                    {props.infoValue}
                </span>
            ) : (
                <span className={styles.normal}>{props.infoValue}</span>
            )}
        </li>
    )
}

InfoRow.propTypes = {
    infoKey: PropTypes.string.isRequired,
    infoValue: PropTypes.any.isRequired,
}

export default InfoRow
