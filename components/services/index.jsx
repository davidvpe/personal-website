import React from 'react'
import PropTypes from 'prop-types'

import styles from './services.module.css'

import ServiceRow from '../services/service'
import Section from '../common/section'
import { data } from './data'

const Services = () => {
    return (
        <Section backTitle='services' title='What I Do?' type='light'>
            {data.map((s, i) => (
                <ServiceRow
                    key={`service-row-${i}`}
                    icon={s.icon}
                    title={s.title}
                    description={s.description}
                />
            ))}
        </Section>
    )
}

export default Services
