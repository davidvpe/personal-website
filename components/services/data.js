import * as Icons from 'react-icons/fa'

export const data = [
    {
        icon: Icons.FaPaintBrush,
        title: 'Web/Mobile UI/UX',
        description:
            'Need a redesign? I can help you with that. After working in several projects that sometimes had a user base of millions I have a clear understanding of what the customer needs and expects to see.',
    },
    {
        icon: Icons.FaLaptopCode,
        title: 'Web Development',
        description:
            'Your business needs a robust website that can deliver speed and trust to your customers, things that a CMS can only delivery up to some level. But with a customized website you can have absolute control over everything.',
    },
    {
        icon: Icons.FaMobile,
        title: 'Mobile Development',
        description:
            "A better way to keep a connection with your customers is to be in their pocket. Don't stay behind other companies who are already there. From simple landing apps to complete apps with different and complex flows",
    },
]
