import React from 'react'
import PropTypes from 'prop-types'

import styles from './service.module.css'

const ServiceRow = (props) => {
    return (
        <div className={styles.container}>
            <div className={styles.icon}>{props.icon()}</div>
            <div className={styles['text-container']}>
                <h1 className={styles.title}>{props.title}</h1>
                <p className={styles.description}>{props.description}</p>
            </div>
        </div>
    )
}

ServiceRow.propTypes = {
    icon: PropTypes.func,
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
}

export default ServiceRow
