import React, { useEffect, useRef } from 'react'
import styles from './home.module.css'
import Typed from 'typed.js'
import { openEmail } from '../../helpers/email'

import RoundedButton from '../common/buttons'

const Home = () => {
    const el = useRef(null)
    const typed = useRef(null)

    useEffect(() => {
        const options = {
            strings: ['David Velarde', 'a Developer', 'a Cook', 'peruvian'],
            typeSpeed: 40,
            backSpeed: 40,
            loop: true,
            backDelay: 1500,
        }
        typed.current = new Typed(el.current, options)

        return () => {
            typed.current.destroy()
        }
    }, [])

    return (
        <section className={styles.background}>
            <div className={styles.overlay}>
                <div className={styles.text_container}>
                    <p className={styles.welcome}>Welcome</p>
                    <p className={styles.animation}>
                        <span>I am </span>
                        <span ref={el}></span>
                    </p>
                    <p className={styles.location}>based in The Netherlands</p>
                    <RoundedButton
                        type='alt'
                        title='Hire me'
                        onClick={openEmail}
                    ></RoundedButton>
                </div>
            </div>
        </section>
    )
}

export default Home
