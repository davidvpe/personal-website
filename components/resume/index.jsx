import React from 'react'
import PropTypes from 'prop-types'
import styles from './resume.module.css'
import Section from '../common/section'
import ResumeItem from './item/item'
import { resume } from './data'
import RoundedButton, { cvHref } from '../common/buttons'
const Resume = (props) => {
    return (
        <Section backTitle='Summary' title='Resume' type='dark'>
            {resume.map((sec, secIndex) => (
                <div
                    className={styles.itemsContainer}
                    title={sec.title}
                    key={`resume-section-${secIndex}`}
                >
                    {sec.items.map((item, itemIndex) => (
                        <ResumeItem
                            key={`resume-item-${itemIndex}`}
                            {...item}
                        />
                    ))}
                </div>
            ))}
            <RoundedButton
                type='greyActive'
                title='Download CV'
                href={cvHref}
            ></RoundedButton>
        </Section>
    )
}

Resume.propTypes = {}

export default Resume
