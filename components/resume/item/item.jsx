import React from 'react'
import PropTypes from 'prop-types'
import styles from './item.module.css'

const ResumeItem = (props) => {
    return (
        <div className={styles.container}>
            <span
                className={styles.dates}
            >{`${props.from} - ${props.to}`}</span>
            <h1 className={styles.title}>{props.title}</h1>
            <h3 className={styles.subtitle}>{props.subtitle}</h3>
            <p className={styles.description}>{props.description}</p>
        </div>
    )
}

ResumeItem.propTypes = {
    from: PropTypes.string.isRequired,
    to: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    subtitle: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
}

export default ResumeItem
