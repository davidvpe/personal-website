export const resume = [
    {
        title: 'My Education',
        items: [
            {
                from: '2009',
                to: '2014',
                title: 'Software Engineering',
                subtitle: 'Universidad Peruana de Ciencias Aplicadas',
                description: 'Top 10th percentile',
            },
        ],
    },
    {
        title: 'My Experience',
        items: [
            {
                from: '2014',
                to: '2017',
                title: 'Software Engineer',
                subtitle: 'Avantica Technologies',
                description:
                    'Worked in different projects for US clientes, mainly on iOS Projects for banking companies',
            },
            {
                from: '2017',
                to: '2019',
                title: 'Software Engineer',
                subtitle: 'Accenture B.V.',
                description:
                    'Moved to The Netherlands, worked in different projects going from a supermarket app, energy company app, and telecommunications app',
            },
            {
                from: '2019',
                to: new Date().getFullYear().toString(),
                title: 'Software Engineer',
                subtitle: 'KPN B.V.',
                description:
                    'Part of the iOS Team that worked on the main app KPN iTV, I worked on the redesign of the app, the kids environment, and the new Player that was launched in 2020',
            },
        ],
    },
]
